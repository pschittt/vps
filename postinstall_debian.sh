#!/bin/sh
# Post installation script debian 6
# upgrade to wheezy/testing

## update apt
apt-get update

## edit and update locale
sed -ire 's/^# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g' /etc/locale.gen
/usr/sbin/locale-gen

## remove unwanted package
apt-get purge apache2* bind9* sasl* fetchmail* xinetd* portmap* samba* sendmail* dhcp3*

## install rcconf pour configurer les applications au démarrage
apt-get install rcconf

## upgrade debian
apt-get upgrade

## update sources.list
cat > /etc/apt/sources.list << EOF
deb http://ftp.fr.debian.org/debian testing main non-free
deb-src http://ftp.fr.debian.org/debian testing main non-free

deb http://security.debian.org/ wheezy/updates main non-free
deb-src http://security.debian.org/ wheezy/updates main non-free
EOF

## update && upgrade to testing
apt-get update
apt-get clean
apt-get upgrade
apt-get install linux-image-amd64
apt-get install udev
apt-get dist-upgrade

## reparer conflits
dpkg --audit
apt-get install --reinstall libssl1.0.0 libudev libavahi-common-data liblzma5 libdb5.1

## Reparer libc6
dpkg-deb -c /var/cache/apt/archives/libc6_2.13-38_amd64.deb | awk {'print $6'} | cut -f2- -d. | sed 's|^/$|/.|' | sed 's|/$||' > /var/lib/dpkg/info/libc6:amd64.list
apt-get install --reinstall libc6


#### fin de l'installation basique

## création d'un utilisateur
adduser franck
usermod -a -G sudo franck

## install git pour récupérer fichiers de configurations
apt-get install git

## as user franck now
## pwd : /home/franck/
mkdir etc_backup_git

## récupère les fichiers etc sauvegardés
git clone https://pschittt@bitbucket.org/pschittt/vps-etc-save.git etc_backup_git/

## isntallation des packages utiles
sudo apt-get install openvpn openvpn-blacklist nginx php5-fpm php5-curl php5-pgsql

## copie des configs
sudo cp -r etc_backup_git/openvpn/ /etc/
sudo cp -r etc_backup_git/nginx/ /etc/
sudo cp -r etc_backup_git/php5/ /etc/

sudo /etc/init.d/openvpn restart
sudo /etc/init.d/nginx restart
sudo /etc/init.d/php5-fpm restart

## fichiers screenrc et vimrc
scp -r -P 40022 .vim .viminfo .vimrc .screenrc vps:/home/franck/

## ssh
mkdir /home/franck/.ssh/
scp .ssh/id_rsa_20130320.pub vps:/home/franck/.ssh/
cat id_rsa_20130320.pub > authorized_keys

## configurer iptables
sudo cp etc_backup_git/network/if-pre-up.d/iptables /etc/network/if-pre-up.d/
sudo chmod +x /etc/network/if-pre-up.d/iptables
sudo /etc/network/if-pre-up.d/iptables

## pour installer tiny tiny rss
apt-get install postgresql
sudo -i -u postgres
psql
ALTER USER postgres WITH ENCRYPTED PASSWORD 'G8xJDBTd_q-p-L';

## pour compiler apache
sudo apt-get install devscripts fakeroot dh-make dpkg-dev
./build_apache.sh
