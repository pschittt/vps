#!/bin/bash

#    debserver9 - a bunch of functions used in the other scripts
#
#    DEBServer9 - Debian Install Server Scripts
#    A set of scripts to automate installation of Servers on Debian
#    Copyright (c) 2016 Frédéric LIETART - stuff@thelinuxfr.org
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

# Syntaxe: # su - -c "./debserver"
# Syntaxe: or # sudo ./debserver
# VERSION="9-20170608"

#=============================================================================
# Liste des applications à installer: A adapter a vos besoins
# Voir plus bas les applications necessitant un depot specifique
LISTE="linux-image-amd64 vim sudo screen ntp htop tree most ccze iftop molly-guard manpages-fr manpages-fr-extra tmux bash-completion needrestart 
wget btrfs-tools python-setuptools curl apt-transport-https ca-certificates software-properties-common"
#=============================================================================

#=============================================================================
# Test que le script est lance en root
#=============================================================================
if [ $EUID -ne 0 ]; then
  echo "Le script doit être lancé en root: # sudo $0" 1>&2
  exit 1
fi
#=============================================================================

#=============================================================================
# Test si version de Debian OK
#=============================================================================
if [ "$(cut -d. -f1 /etc/debian_version)" == "9" ]; then
				echo "Version compatible, début de l'installation"
else
        echo "Script non compatible avec votre version de Debian" 1>&2
        exit 1
fi
#=============================================================================

#=============================================================================
# Mise a jour de la liste des depots
#=============================================================================
echo "
deb http://deb.debian.org/debian/ stretch main contrib non-free
#deb-src http://deb.debian.org/debian/ stretch main contrib non-free

deb http://deb.debian.org/debian/ stretch-updates main contrib non-free
#deb-src http://deb.debian.org/debian/ stretch-updates main contrib non-free

deb http://deb.debian.org/debian-security stretch/updates main
#deb-src http://deb.debian.org/debian-security stretch/updates main" > /etc/apt/sources.list
#=============================================================================

#=============================================================================
# Update
#=============================================================================
echo -e "\033[34m========================================================================================================\033[0m"
echo "Mise à jour de la liste des dépôts"
echo -e "\033[34m========================================================================================================\033[0m"
apt-get update
clear
#=============================================================================

#=============================================================================
# Upgrade
#=============================================================================
echo -e "\033[34m========================================================================================================\033[0m"
echo "Mise à jour du système"
echo -e "\033[34m========================================================================================================\033[0m"
apt-get upgrade
clear
#=============================================================================

#=============================================================================
# Installation
#=============================================================================
echo -e "\033[34m========================================================================================================\033[0m"
echo "Installation des logiciels suivants: $LISTE"
echo -e "\033[34m========================================================================================================\033[0m"
apt-get -y install $LISTE
#=============================================================================

#=============================================================================
# Install Docker
#=============================================================================
echo -e "Installing Docker"
wget https://download.docker.com/linux/debian/gpg
apt-key add gpg
# Docker
echo "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" >> /etc/apt/sources.list
apt-get update
apt-get install -y docker-ce docker-compose
#=============================================================================

#=============================================================================
# Configuration bashrc
#=============================================================================
cat > "$HOME"/.bashrc << EOL
#-------------------
# Alias
#-------------------
# la couleur pour chaque type de fichier, les répertoires s'affichent en premier
alias ls='ls -h --color --group-directories-first'
# affiche les fichiers cachés
alias lsa='ls -A'
# affiche en mode liste détail
alias ll='ls -ls'
# affiche en mode liste détail + fichiers cachés
alias lla='ls -Al'
# tri par extension
alias lx='ls -lXB'
 # tri par taille, le plus lourd à la fin
alias lk='ls -lSr'
# tri par date de modification, la plus récente à la fin
alias lc='ls -ltcr'
# tri par date d’accès, la plus récente à la fin
alias lu='ls -ltur'
# tri par date, la plus récente à la fin
alias lt='ls -ltr'
# Pipe a travers 'more'
alias lm='ls -al | more'
# ls récurssif
alias lr='ls -lR'
# affciche sous forme d'arborescence, nécessite le paquet tree
alias tree='tree -Csu'
# affiche les dernière d'un fichier log (par exemple) en live
alias voirlog='tail -f'
# commande df avec l'option -human
alias df='df -kTh'
# commande du avec l'option -human
alias du='du -kh'
# commande du avec l'option -human, au niveau du répertoire courant
alias duc='du -kh --max-depth=1'
# commande free avec l'option affichage en Mo
alias free='free -m'
# nécessite le paquet "htop", un top amélioré et en couleur
alias top='htop'
# faire une recherche dans l'historique de commande
alias shistory='history | grep'
# raccourci history
alias h='history'

# Ajout log en couleurs
ctail() { tail -f \$1 | ccze -A; }
cless() { ccze -A < \$1 | less -R; }

#set a fancy prompt (non-color, unless we know we want color)
PS1="\\\[\\\033[01;31m\\\][\\\u@\\\h\\\[\\\033[00m\\\]:\\\[\\\033[01;34m\\\]\\\w]\\\[\\\033[00m\\\]\\\$ "

# activation date_heure dans la commande history
export HISTTIMEFORMAT="%Y/%m/%d_%T : "

# les pages de man en couleur, necessite le paquet most
export PAGER=most

# enable bash completion in interactive shells
if [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
fi
EOL

clear
#=============================================================================

#=============================================================================
# Email admin
#=============================================================================
echo -ne "\033[32;1mAdresse mail pour les rapports de sécurité: \033[0m"
read -r MAIL
#=============================================================================

#=============================================================================
# Reconfigure locales !
#=============================================================================
echo -ne "\033[32;1mReconfiguration des locales en fr_FR.UTF-8\033[0m"
sed -ire 's/^# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/g' /etc/locale.gen
/usr/sbin/locale-gen
#=============================================================================

#=============================================================================
# Désactiver les paquets recommandés !
#=============================================================================
echo -ne "\033[31;1mATTENTION : Voulez-vous désactiver l'installation de paquets recommandés (y/N): \033[0m"
read -r NORECOMMENDS
: "${NORECOMMENDS:="N"}"
if [[ ${NORECOMMENDS} == [Yy] ]]; then
    echo "APT::Install-Recommends \"0\";
    APT::Install-Suggests \"0\"; " > /etc/apt/apt.conf
fi
#=============================================================================
clear

#=============================================================================
# Configuration cron-apt
#=============================================================================
echo -ne "\033[32;1mVoulez-vous installer cron-apt (y/N): \033[0m"
read -r CRONAPT
: "${CRONAPT:="N"}"
if [[ ${CRONAPT} == [Yy] ]]; then
    apt-get -y install -y cron-apt
	echo "
APTCOMMAND=/usr/bin/apt-get
MAILTO=\"$MAIL\"
MAILON=\"upgrade\"" > /etc/cron-apt/config
echo -ne "\033[32;1mVoulez-vous installer les mises à jours automatiquement (Y/n): \033[0m"
read -r CRONAPTAUTO
: "${CRONAPTAUTO:="Y"}"
if [[ ${CRONAPTAUTO} == [Yy] ]]; then
    echo "dist-upgrade -y -o APT::Get::Show-Upgraded=true" > /etc/cron-apt/action.d/5-install
fi
fi
#=============================================================================

#=============================================================================
# Install Webmin
#=============================================================================
echo -ne "\033[32;1mVoulez-vous installer Webmin (y/N): \033[0m"
read -r WEBMIN
: "${WEBMIN:="N"}"
if [[ ${WEBMIN} == [Yy] ]]; then
	wget http://prdownloads.sourceforge.net/webadmin/webmin_1.801_all.deb &&
	dpkg --install webmin_1.801_all.deb ||
	apt-get install -fy &&
	rm webmin_1.801_all.deb
fi
#=============================================================================

#=============================================================================
# Install Glances
#=============================================================================
##echo -ne "\033[32;1mVoulez-vous installer Glances (y/N): \033[0m"
##read -r GLANCES
##: "${GLANCES:="N"}"
##if [[ ${GLANCES} == [Yy] ]]; then
##  echo -ne "\033[32;1mVoulez-vous installer Glances via PIP (y/N): \033[0m"
##  read -r GLANCESPIP
##  if [[ ${GLANCESPIP} == [Yy] ]]; then
##	   wget -O- http://bit.ly/glances | /bin/bash
##    else apt-get install -y glances
##  fi
##fi
#=============================================================================

#=============================================================================
# Install cheat (via pip) https://github.com/chrisallenlane/cheat
#=============================================================================
echo -ne "\033[32;1mVoulez-vous installer Cheat (via pip) (y/N): \033[0m"
read -r CHEAT
: "${CHEAT:="N"}"
if [[ ${CHEAT} == [Yy] ]]; then
	apt-get install -y python-pip
  pip install cheat
fi
#=============================================================================

#=============================================================================
# Install UFW
#=============================================================================
echo -ne "\033[32;1mVoulez-vous installer UFW (y/N): \033[0m"
read -r UFW
: "${UFW:="N"}"
if [[ ${UFW} == [Yy] ]]; then
	apt-get install -y ufw
    ufw allow ssh
    ufw logging on
    ufw enable
fi
#=============================================================================

#=============================================================================
# Install fail2ban & rkhunter
#=============================================================================
echo -ne "\033[32;1mVoulez-vous installer des systèmes anti-malwares/bruteforce (y/N): \033[0m"
read -r FAIL2BAN
: "${FAIL2BAN:="N"}"
if [[ ${FAIL2BAN} == [Yy] ]]; then
	apt-get install -y fail2ban rkhunter
  dpkg-reconfigure rkhunter
fi
#=============================================================================


#=============================================================================
# Ajout d'un utilisateur
#=============================================================================
echo -ne "\033[32;1mVoulez-vous ajouter un utilisateur (y/N): \033[0m"
read -r USER
: "${USER:="N"}"
if [[ ${USER} == [Yy] ]]; then
	echo -ne "\033[32;1mEntrez le nom d\'utilisateur': \033[0m"
	read -r USRNAME
	echo -ne "\033[32;1mEntrez le mot de passe': \033[0m"
	read -r PWD
	# quietly add a user without password
    adduser --quiet --disabled-password --shell /bin/bash --home /home/${USRNAME} --gecos "User" ${USRNAME}
    # set password
    echo "${USRNAME}:${PWD}" | chpasswd
    echo -ne "Ajout de l\'utilisateur dans sudo"
    usermod -a -G sudo ${USRNAME}
    echo '${USRNAME}  ALL=(ALL:ALL) ALL' >> /etc/sudoers
    echo 'Defaults env_keep' >> /etc/sudoersers
    echo -ne "Ajout au groupe Docker"
    groupadd docker
    usermod -a -G docker ${USRNAME}
    cp .bashrc /home/${USRNAME}/
fi
#=============================================================================

#=============================================================================
# Reconfigure openssh-server !
#=============================================================================
echo -ne "\033[32;1mSécurisation de la conf SSH :\033[0m"
echo -ne "
Port 40022
PermitRootLogin no
PermitEmptyPassword no
X11Forwarding no
AllowUsers ${USER}
ClientAliveInterval 360
CLientAliveCountMax 0
Protocol 2"

sed -i "s/#Port 22/Port 40022/g" /etc/ssh/sshd_config
sed -i "s/PermitRootLogin yes/PermitRootLogin no/g" /etc/ssh/sshd_config
sed -i "s/#PermitEmptyPassword no/PermitEmptyPassword no/g" /etc/ssh/sshd_config
sed -i "s/X11Forwarding yes/X11Forwarding no/g" /etc/ssh/sshd_config
echo "AllowUsers ${USRNAME}" >> /etc/ssh/sshd_config
echo 'ClientAliveInterval 360' >> /etc/ssh/sshd_config
echo 'ClientAliveCountMax 0' >> /etc/ssh/sshd_config
echo 'Protocol 2' >> /etc/ssh/sshd_config

ufw allow 40022/tcp
systemctl restart ssh.service

#=============================================================================

#=============================================================================
# END
#=============================================================================
clear
echo -e "\033[34m========================================================================================================\033[0m"
echo "Liste d'applications utiles installées :"
echo "$LISTE"
echo ""
echo "Pensez à aller dans /etc/default pour configurer les daemons smartmontools hdparm"
echo ""
echo "Notes de publication : https://www.debian.org/releases/stretch/releasenotes"
echo "Manuel d'installation : https://www.debian.org/releases/stretch/installmanual"
echo ""
echo "Le cahier de l'administrateur Debian : https://debian-handbook.info/browse/fr-FR/stable/"
echo -e "\033[34m========================================================================================================\033[0m"
#=============================================================================